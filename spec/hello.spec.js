let hello = require('../src/hello')

describe("Testing 'hello' function", function() {
  it("Hello, World!", function() {
    expect(hello()).toBe('Hello, World!')
  })

  it("Hello, pietrom!", function() {
    expect(hello('pietrom')).toBe('Hello, pietrom!')
  })
})